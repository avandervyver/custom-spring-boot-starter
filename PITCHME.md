---
@snap[west headline]
## Spring Boot Autoconfiguration 
@snapend

@snap[south-west byline]
### "You Autoconfigured WHAAT?"
@snapend
---
### At what will we be looking?
1. Introduction
1. What is the problem Spring Boot solves?
1. Spring Boot Autoconfiguration: Opening the Black Box
1. Custom Spring Boot Starter
---
### Spring’s Objective: 
** DRY SOCs? ** 
* Remove boilerplate code.
* Separation of Concerns: Bussiness Logic vs Configuration.
* Wants the developer to focus on implementing the business logic 
---
### Spring's Developers Reputation History
From "Copy and Paste XML Coders" to "Copy and Paste Java Configuration Coders"
---
### Problem:
* Spring's Configuration became the boilerplate code.
* Configuring a new Spring Application could have taken days
* Can be a major obstacle when starting with Spring.
---
## Solution
# Spring Boot!
---
### But before Spring Boot could happen?
 * One piece of functionality was missing 
---
### Spring 4 release
 * Added @Conditional
 * Gives us the option to create beans based on a set of conditions.
---
### Spring Boot is **NOT**
* Code Generation
* a "Plugin."
---
### **It is:** ###
 * An opinionated - extension of Spring pre-configured Beans.

Note:
"Opinionated": This is in contrast with Spring's original design methodology. They provide everything; you decided what is best.
---
### Advantages:
  * Easy and Fast to get started
  * Removes the Learning Curve of getting started with Spring!
  * It does the copy and paste of configurations for you!

*Ps. Spring Boot was not the first attempt! (Remember Spring Roo?)*
---
### Disadvantages:
  * It is Magic!
  * All fun and games until things go wrong!
  * You do lose touch with what is happening.
---
### Magic is easy to Understand
 * Just a couple of concepts
---
### Your first Spring Boot Project.
* Create your project
* Added a couple of dependencies
* And BOOM... you have something up and running?
Note:
Just a quick overview of what Jorian did?
---
### However, how does it work?
---
### Spring Boot Startup Overview
1. Application Startup
1. Scans a list of autoconfiguration
1. Each autoconfiguration contains a list of conditional beans. 
1. Bean Properties are created with default values
1. Beans Properties are then overwritten with the application.properties. 
1. Beans are created 
---
### Classpath
* Maven / Gradle / Any + Ivy
* Alternatively, Whatever! 
* It just copies Jar to the classpath!
---
### Spring-Boot-Starters
```
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```
> Starters contain a lot of the dependencies that you need to get a project up and running quickly and with a consistent, supported set of managed transitive dependencies.

Note:
* [More Starters available](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using-boot-starter)
---
### spring.factories
```
# Auto Configure
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration,\
org.springframework.boot.autoconfigure.aop.AopAutoConfiguration,\
org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration,\
org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration,\
org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration,\
org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration,\
org.springframework.boot.autoconfigure.cloud.CloudServiceConnectorsAutoConfiguration,\
org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration,\
org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration,\
org.springframework.boot.autoconfigure.couchbase.CouchbaseAutoConfiguration,\
org.springframework.boot.autoconfigure.dao.PersistenceExceptionTranslationAutoConfiguration,\
org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration,\

```
---
### AutoConfiguration
It is just a class containing a set of beans with default properties. These pre-configured beans are activated by a set of conditions.
---
### Conditional Annotations
* ConditionalOnBean
* ConditionalOnClass
* ConditionalOnExpression
* ConditionalOnMissingBean
* ConditionalOnProperty
* Many more! (org.springframework.boot.autoconfigure.condition)

---
### Application Properties
 * Override the default settings
```
server.port=8080
spring.mustache.prefix=./pages/
spring.mustache.suffix=.html
```
---
### @Value
* The standard mechanism to read property files.

---
### Bonus Question: For **10 "points"**!! 
 * What bean is created by Spring Boot to "handle" property files?

Note:
PropertyPlaceholderConfigurer 
---
### Problem?
* Using too many @Value annotations?

```
public class FlywayProperties {
    @Value("${enabled}")
    private boolean enabled = true;
    @Value("${checkLocation}")
    private boolean checkLocation = true;
    @Value("${encoding}")
    private Charset encoding = StandardCharsets.UTF_8;
    @Value("${connectRetries}")
    private int connectRetries;
    @Value("${table}")
    private String table = "flyway_schema_history";
```
---
### ConfigurationProperties

```
@ConfigurationProperties(prefix = "spring.flyway")
public class FlywayProperties {
      private boolean enabled = true;
...
// Getters and Setters
```
 * Application.properties
```
spring.flyway.enable=false
```
---
### How can we use it?
1. Add it to any of your POJOs. 
1. Create a Bean of a third party + @ConfigurationProperties annotation
---
### Demo
Note:
Look at: H2ConsoleAutoConfiguration
---
### Cheat sheet for Custom Starter
1. Library Implementation + Configuration
1. Auto Configuration + Default Property Configuration.
1. Add spring.factories to your AutoConfiguration Project
1. Create your starter pom.xml 
---
### Demo: Custom Spring Boot Starter
---
## Handy Features:
 * debug=true
 * Actuator Endpoints
 * banner.txt
 * Fat Jars
---
### Overview
* Why Spring Boot?
* How does autoconfiguration work?
* How can we create our *-spring-boot-starter
---
### Thank you!
* Slides at http://tiny.cc/76ft2y
